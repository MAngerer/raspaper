\section{Background} \label{sec:background}
In this section we briefly introduce the main theoretical concepts of the port-Hamiltonian framework.
% An objective is to achieve the two outlined tasks, while ensuring passivity of the system even if the passivity of the human is not assumed. More specifically, we want to guarantee the following inequality for the system is always valid:
% \begin{eqnarray}
% \dot{\mathcal{H}_s} + \dot{\mathcal{H}_c} + \dot{\mathcal{H}_t} \leq \f{u}^T\f{y}
% \end{eqnarray}
%update io of the eq.
%write in more detail
\subsection{port-Hamiltonian framework}
The port-Hamiltonian framework is based on a known energy function, the \emph{Hamiltonian}~($\mathcal{H}$), and provides the energy-consistent description of a physical system~\cite{vanderSchaft_06}. As the power conservative interconnection of port-Hamiltonian systems is again a port-Hamiltonian system, this framework is suitable for modeling complex and interconnected systems. 
The modeling of mechanical systems in the port-Hamiltonian framework can be achieved by interconnecting atomic structure elements: inertias, dampers and springs. Every atomic element is defined by a Hamiltonian energy function and interacts with the other elements by exchanging energy through a \emph{port}. The port is described by a pair of variables $(e.f)$ representing \emph{efforts} and \emph{flows}, respectively. Their dual product is power.\\ 
An input-state-output form of a port-Hamiltonian system with the Hamiltonian function $\mathcal{H}$ is:
\begin{eqnarray}\label{EQ:generalPHS}
\begin{aligned}
\dot{\f{x}} &= [J(\f{x})-D(\f{x})]\frac{\partial \mathcal{H}}{\partial \f{x}}(\f{x}) + G(\f{x})\f{u}\\
\f{y} &= G^T(\f{x})\frac{\partial \mathcal{H}}{\partial \f{x}}(\f{x}),
\end{aligned}
\end{eqnarray}
where $J(\f{x})$ is a skew-symmetric structure matrix and $D(\f{x})  \succeq 0$ is a symmetric dissipation matrix. Inputs to the system are flows, $\f{u}$, and outputs are efforts $\f{y}$. The matrix $G(\f{x})$ is a mapping matrix and $\f{x}$ is the state vector of the system. The Hamiltonian of the system is a positive, semi-definite function $\mathcal{H} \geq 0 $, which represents the total energy stored in the system. The rate of the energy change of~(\ref{EQ:generalPHS}) is:
\begin{eqnarray}\label{EQ:E_balance_generalPHS}
\dot{\Ha} = \f{y}^T\f{u} - \frac{\partial^T \mathcal{H}}{\partial \f{x}}D(\f{x})\frac{\partial \mathcal{H}}{\partial \f{x}}
\end{eqnarray}
According to~(\ref{EQ:E_balance_generalPHS}) every port-Hamiltonian system described in the form~(\ref{EQ:generalPHS}) is passive.

\subsection{Twists and wrenches}
We use coordinate frames associated with a body, i.e. non-inertial frames. A transformation from the inertial frame, $\Psi_0$, to a frame $\Psi_i$ is given by a homogeneous matrix:
\begin{eqnarray}\label{EQ:coordchange}
	H_0^i := \begin{pmatrix*}[l]R_0^i & \f{p}_0^i \\ \f{0}_{1\times3} & 1\end{pmatrix*} \in SE(3)
\end{eqnarray}
which consists of a translation vector $\f{p}_0^i \in \mathbb{R}^3$ and a rotation matrix $ R_0^i \in SO(3)$.\\
In this paper we represent the rigid body motion with the twist notation (see e.g. \cite{Stramigioli_01}). 
The twist associated with the frame $\Psi_i$, relative to the frame $\Psi_j$ and expressed in the inertial frame $\Psi_0$ is: 
\begin{eqnarray}
T_i^{0,j} = \begin{pmatrix} (\f{\omega}_i^{0,j})^T  \quad (\f{v}_i^{0,j})^T\end{pmatrix}^T
\end{eqnarray}
and consists of an angular velocity $\f{\omega}_i^{0,j} \in \mathbb{R}^3$  around a screw axis and a translational velocity $\f{v}_i^{0,j} \in \mathbb{R}^3$ along the same axis.
A coordinate transformation of a twist from $\Psi_0$ to $\Psi_j$ is given by
\begin{eqnarray}
T_i^{j,j} = T_i^j = Ad_{H_0^j} T_i^{0,j} = \begin{pmatrix}R_0^j & 0 \\ \tilde{p}_0^j R_0^j & R_0^j\end{pmatrix} T_i^{0,j},
\end{eqnarray}
where $\tilde{p}_0^j$ is the skew-symmetric matrix of $\f{p}_0^j$. The dual quantity of a twist is a \emph{wrench}. The wrench $W_i^0$ which acts on a body associated with the frame $\Psi_i$, expressed in the inertial frame $\Psi_0$ is:
\begin{eqnarray}
W_i^0 = \begin{pmatrix} (\f{m}_i^0)^T \quad (\f{f}_i^0)^T \end{pmatrix}^T
\end{eqnarray}
where $ \f{m}_i^0 \in \mathbb{R}^3 $ are moments and $ \f{f}_i^0 \in \mathbb{R}^3$ are forces. A coordinate transformation of a wrench from $\Psi_0$ to $\Psi_j$ is defined as:
\begin{eqnarray}
W_i^j = Ad_{H_j^0}^T W_i^0
\end{eqnarray}

\subsection{port-Hamiltonian formulation of mechanical elements}\label{SS:pHElements}
Let us define the atomic mechanical elements in the port-Hamiltonian form.
\subsubsection{Springs}
A spring is defined between two bodies, $ B_i $ and $ B_j $,  with coordinate frames $ \Psi_i $ and $ \Psi_j $ fixed to the bodies, respectively. The potential energy stored in the spring is a positive definite function of the displacement $H_i^j$:
\begin{equation}\label{EQ:SpringEnergyFunction}
	\Ha_{\g{P}} : SE(3) \rightarrow \mathbb{R}^+; \; H_i^j \mapsto \Ha_{\g{P}}(H_i^j).
\end{equation}
Energy functions of different types of springs are summarized in \cite{Stramigioli_01}.
The input-state-output form of the spring is defined by the displacement $H_i^j$ (state), the wrench $W_i^{i}$ (output) and the twist $T_i^{i,j}$ (input):
\begin{eqnarray}
\begin{aligned}
	&\dot{H}_i^j = H_i^j T_i^{i,j}\\
	&W_i^{i} = {H_i^j}^T \frac{\partial \Ha_{\g{P}}(H_i^j)}{\partial H_i^j}.
\end{aligned}
\end{eqnarray}
\subsubsection{Inertias}
The kinetic energy stored in a body is a function of its relative motion w.r.t. to an inertial reference frame:
\begin{eqnarray}
\Ha_\g{K}(P_b^b) = \frac{1}{2}(P_b^b)^T M_b^{-1} P_b^b
\end{eqnarray}
where $P_b^b$ is the momentum of a body $b$ expressed in a body-fixed frame $\Psi_b$ and $M_b$ is the inertial matrix of the body. 
%twist and momentum are related by $P_b^b = M_b T_b^{b,0}$. Whenever motion is expressed in a non-inertial frame fictitious forces (Coriolis, centrifugal) need to be considered. 
The input-state-output form of the body is defined by the momentum $P_b^b$ (state), the twist $W_i^{j}$ (output) and the twist $T_i^j$ (input):
\begin{eqnarray}\label{EQ:pHInertia}
\begin{aligned}
	&\dot{P_b^b} = C_b \frac{\partial \Ha_\g{K}(P_b^b)}{\partial P_b^b} + I_6 W_{b}^b \\
	&T_b^{b,0} = I_6 \frac{\partial \Ha_\g{K}(P_b^b)}{\partial P_b^b}
\end{aligned}
\end{eqnarray}
where $C_b$ represents Coriolis and centrifugal forces, $I_6$ is the identity matrix of order $6$.
\subsubsection{Dampers}\label{SSS:Dampers}
Energy dissipation is represented as an input-output mapping between wrenches and twists:
\begin{eqnarray}
W_b^b = F(T_b^{b,0}).
\end{eqnarray}
For linear dampers, $D$, the wrench is directly proportional to the twist:
\begin{equation}
	W_b^b = D T_b^{b,0},
\end{equation}
%or a damper in parallel with spring
%\begin{equation}
%	W_i^{j,j} = D T_i^{j}.
%\end{equation}
The dissipated (co-)energy in this case is:
\begin{eqnarray}
\Ha_\g{D}^* = \frac{1}{2}{T_b^{b,0}}^T D T_b^{b,0}.
\end{eqnarray}
%\begin{figure}[h!]
%	\centering
%	\footnotesize
%	\def\svgwidth{0.8\columnwidth}
%%	\includegraphics[]{model.eps}
%    \input{pics/model.eps_tex}
%	\caption{Cooperative system model represented as a set of interconnected energy storing elements, inertias and a spring.}
%	\label{FIG:model}
%	\vspace{-10pt}
%\end{figure}