\section{Control strategy for human-robot team interaction}\label{sec:control}
%Additionally, we define frames at the contact points of the object as $\Psi_{b_i}$. 
In this section we propose a passivity-based control methodology within the port-Hamiltonian framework for the human-robot team interaction in which the human commands the overall behavior of the team, while the robot team manipulates and maintains the grasp of the object. The control approach is a cascade of energy shaping control strategy with the damping injection and the energy transfer control.\\
Energy shaping is achieved by exploiting the available information on the energetic state obtained from the controller and the modeled system. A maximum level of the energy to be stored in the controller and the system limits both the velocity and the forces. The energy bound of the controller-system interconnection is ensured by sourcing it from an energy tank. The block structure of the complete system is depicted in fig.~(\ref{FIG:BlockScheme}).
\begin{figure*}[t!]
\centering
\small
\include{pics/blockscheme}
\vspace{-10pt}
\caption{Block structure of the complete system:  The Energy tank supplies energy to the Controller, while the human is energetically decoupled (indicated by dotted lines). The energy flow depends on the human input and is controlled via a Modulated Transformer~($\mathbb{MTF}$). The output of the transformer is the (modified) reference trajectory $T_d^0$. The wrench applied by the end-effectors is $W_i^0$ and the twists of the end-effectors are $T_i^{0}$.}
\label{FIG:BlockScheme}
\vspace{-10pt}
\end{figure*} 
\subsection{Energy shaping and damping injection for cooperative manipulation}
The controller we propose has a physical interpretation since it is based on the virtual mechanical structures assumed to interconnect the robots and the human with the object. Therefore, the controller can be represented in the port-Hamiltonian framework. There are two alternatives to establish a desired formation of robots around the held object: $1$) the use of non-zero rest-length springs connecting the robots pairwise, or $2$) the introduction of a virtual object as a hinge point~\cite{99stramigioli}. We use the virtual object concept.
%why? internal wrenches!
The virtual object is connected by a virtual spring and a virtual damper to the human hand. In subsection \ref{SS:EnergyAdaptedStiffness} we introduce variable stiffness and damping parameters to shape the energy flow towards the controller, affected by the human commands. The end-effectors are coupled to the surface of the virtual object with constant virtual springs and virtual dampers. The proposed virtual structure is depicted in fig.~(\ref{FIG:controller}).\\
\begin{figure}[h!]
	\centering
	\footnotesize
	\def\svgwidth{0.7\columnwidth}
    \input{pics/admittancecontrol_new.eps_tex}
	\caption{Interaction controller represented as a virtual structure of inertia, (variable) springs and (variable) dampers.}
	\label{FIG:controller}
	\vspace{-10pt}
\end{figure}
Due to space constraints only an interconnection of the $i$-th manipulator is given in the equations of the controller. Their extension to $N$ interconnections is straightforward. We represent the controller in the port-Hamiltonian framework as:
\begin{eqnarray}\label{EQ:constrainedPHS}
\begin{aligned}
\dot{\fx}_c &= [J_c(\fxc)-D_c(\fxc)]\frac{\partial \mathcal{H}_c}{\partial \fxc}(\fxc) + G_c(\fxc)\fuc\\
\fyc &= G_c^T(\fxc)\frac{\partial \mathcal{H}_c}{\partial \fxc}(\fxc)\\
\end{aligned}
\end{eqnarray}
with the components:
\begin{gather}
\begin{aligned}
&\dot{\fx}_c= \begin{pmatrix*}[l] \dot{H}_v^d \\  \dot{P_v^v} \\ \dot{H}_v^i\end{pmatrix*}, 
\frac{\partial \mathcal{H}_c}{\partial \fxc} = \begin{pmatrix}\frac{\partial \Ha_c}{\partial H_v^d} \\ \frac{\partial \Ha_c}{\partial P_v^v} \\ \frac{\partial \Ha_c}{\partial H_v^i}\end{pmatrix}, 
\fuc = \begin{pmatrix}
T_d^0 \\ T_i^0
\end{pmatrix},
\fyc = \begin{pmatrix}
W_d^{0} \\ W_i^{0}
\end{pmatrix}
\\
&J_c = \begin{pmatrix}  0 & H_v^d & 0  \\
 -{H_v^d}^T & C_v & -{H_v^i}^T  \\
 0 & H_v^i & 0 
\end{pmatrix}, \\
&D_c = \begin{pmatrix}  0 & 0 & 0  \\
 0 & D_v+D_i+D_0 & 0  \\
 0 & 0 & 0  \\
\end{pmatrix}, \\
&G_c= \begin{pmatrix} G_{c_1 } & G_{c_2} \end{pmatrix}
 = \begin{pmatrix}
 -H_v^d Ad_{H_0^v} & 0 \\
  D_v Ad_{H_0^v}  & D_i Ad_{H_0^v} \\
 0 & -H_v^i Ad_{H_0^v} 
\end{pmatrix}.
\end{aligned}
\raisetag{2em} 
\end{gather}. 
The state vector $\fxc$ is a stacked vector of:
\begin{enumerate}
\item the variable virtual spring, ${H}_v^d$, connecting the human hand and the virtual object,
\item the virtual object $v$ momenta, $P_v^v$, and
\item the virtual spring, ${H}_v^i$, connecting the virtual object and the $i$-th end-effector.
\end{enumerate}
The Hamiltonian energy of the controller is the sum of the energies stored in all its atomic elements:
\begin{eqnarray}
\Ha_c(\fxc) = \Ha_\g{P}(H_v^d) + \sum_{i=1}^N \Ha_\g{P}(H_v^i) + \Ha_\g{K}(P_v^v).
\end{eqnarray}
The controller is energy-conservative and due to the assigned damping it is also passive. It is suitable for achieving both the desired velocity set-point and formation preservation of the robots. The formation preservation ensures that the constraints are not violated. In the next subsection the interconnection of the model and the controller is studied.
\subsection{Connection of the controller and the model}
The output of the controller, $W_i^0$, is the input for the model and the output of the model, $T_i^0$, is the input for the controller. Therefore, the interconnected system is:
\begin{gather}
\begin{aligned}
\underbrace{\begin{pmatrix}
\dot{\hat{\fx}} \\ \dot{\fx}_c 
\end{pmatrix}}_{\dot{\fx}_{cs}} 
&=
\underbrace{\begin{pmatrix}
\hat{J} & \hat{G}_\g{m} G_{c2}^T \\ -G_{c2} \hat{G}_\g{m}^T & J_c-D_c 
\end{pmatrix}}_{J_{cs}}
\underbrace{\begin{pmatrix}
\frac{\partial \Ha_{cs}}{\partial \hat{\fx}} \\ \frac{\partial \Ha_{cs}}{\partial \fxc}
\end{pmatrix}}_{\partial \Ha_{cs}/\partial \fx_{cs}}
+ \\
&+ \underbrace{\begin{pmatrix}
\hat{G}_b & G_{c1}
\end{pmatrix}}_{{G_{cs}}}
\begin{pmatrix}
W_b^0 \\ T_d^{0}
\end{pmatrix}
\\
\begin{pmatrix}
T_b^{0} \\ W_d^0
\end{pmatrix}
&=
\begin{pmatrix}
\hat{G}_b^T & G_{c1}^T
\end{pmatrix}
\begin{pmatrix}
\frac{\partial \Ha_{cs}}{\partial \hat{\fx}} \\ \frac{\partial \Ha_{cs}}{\partial \fxc}
\end{pmatrix}
\end{aligned}
\raisetag{2em}
\label{EQ:pH_model_controller}
\end{gather} 
The external ports of the controller-model interconnection are for interaction with the human via the  port ($T_d^{0}, W_d^0$) and for interaction with the environment via the port ($W_b^0, T_b^{0}$), i.e. via the object. The interconnection given with eq.~(\ref{EQ:pH_model_controller}) is passive as the interconnected subsystems are passive. \\
%The controller is therefore passive and we call it a~\emph{constrained dynamic intrinsically passive controller (constrained dIPC)}. Due to the passivity an external source is necessary to perform work and execute tasks. This role is taken by the human operator. The next section proposes an energy regulation strategy to guarantee stability regardless of the behaviour of the humans. 
%A model based controller is described in the port-Hamiltonian framework as an interconnection of virtual mechanical elements. It is suitable for achieving both trajectory tracking and formation preservation of the robots. The formation preservation ensures that the excessive stress is not applied on the object.
In the next subsection we energetically decouple the human guiding the cooperative system to ensure passivity of the complete system and safety for humans on-site.
\subsection{Energy transfer control}\label{S:EnergyTanks}
As the human and the system are not physically coupled, but the human commands the system with hand gestures, we interconnect them with virtual structures. 
%Precision can be obtained with high stiffness if relatively low velocities are commanded. The motion over a relatively large distance can be achieved with low stiffness, if relatively high velocities are commanded and the precision is not the primary goal. This implies that the virtual variable stiffness is suitable for interconnecting the human and the system. However, the variability of the virtual structures affects the stability of the system. Additionally, since we assume human(s) and the robots share the common workspace, it is necessary to additionally ensure human safety.\\
In order to guarantee both stability and safety, we propose an energy transfer control in the form of an energy tank that supplies the controller and the system. The energy tank maintains a safe energy level in the system and can be integrated into the port-Hamiltonian representation of the complete, controlled system. \\
For human-robot interaction various safety metrics exist, see~\cite{Haddadin_14} for an overview. In this paper we limit the focus to the energy-based injury criteria. Experimental studies indicate minimal amounts of energy that cause a cranial bone failure~\cite{Wood_71} and a fracture of neck bones~\cite{Yoganandan_96}. Based on these results, we define a maximum amount of energy that can be stored in the system. It represents the maximum level of the energy that can be stored in the tank:
\begin{equation*}
\mathcal{H}_t^{max} = 
\begin{cases}
517 \; \text{J}  & \text{adult cranium bone failure}\\
30 \;  \text{J} & \text{neck fracture}\\
\end{cases}
\label{EQ:safety_limit}
\end{equation*}
%We propose to assign an energy budget to the operator at disposal to issue commands. Therefore we introduce an energy tank to source the controller, leaving the operator energetically decoupled, but in control of the energy transfer.
%The motivation for the use of an \emph{energy tank} is to energetically decouple the human operator from the robotic system, i.e. the operator is no longer able to supply energy to the system. %This is a consequence of the chosen input method, which is based on information rather than on energy exchange, as discussed above.
%Therefore, the energy necessary for driving the robotic system comes from the tank while the human operator controls the energetic flow. 
%The scheme is illustrated in figure \ref{FIG:tanksystem}. \\
 %\begin{figure}
%		\centering
%		\footnotesize
%		\def\svgwidth{1\columnwidth}
%		\includegraphics[]{tanksystem.eps}
 %       %input{tanksystem.eps_tex}
  %      \caption{The energy tank supplies the passive controller, while the human user is energetically decoupled but controls the flow of energy. All energy to the robots flows through the controller. The model-based controller monitors the energetic state and limits the energy supply if necessary to ensure stable operation regardless the human behaviour.}
   %     \label{FIG:tanksystem}
%\end{figure}
Let us formally analyze the combination of the energy tank and the controlled system,~(\ref{EQ:pH_model_controller}). The energy tank sources the controller and the controller resupplies its virtually dissipated energy to the energy tank.
The energy tank is a virtual storage element defined with the Hamiltonian, $\mathcal{H}_t$. Let $x_\g{t} \in \mathbb{R}$ denote the (scalar) energy state of the tank and let $\mathcal{H}_t(x_\g{t}) = \frac{1}{2}x_\g{t}^2$. The input-state-output, port-Hamiltonian representation of the energy tank is: %reference
\begin{eqnarray}
\begin{aligned}
\dot{x_\g{t}} &= f_\g{t} \\
e_\g{t} &= \frac{\partial \mathcal{H}_t(x_\g{t})}{\partial x_\g{t}} (=x_\g{t})
\end{aligned}
\end{eqnarray}
where $(f_\g{t},e_\g{t})$ is the flow-effort pair. 
The re-routing of the virtually dissipated energy into the tank is accomplished by choosing the tank input as:
\begin{equation}
 f_\g{t} = \frac{1}{x_\g{t}}\frac{\partial^T \mathcal{H}_{cs} }{\partial \f{x}_{cs} }D_c\frac{\partial \mathcal{H}_{cs} }{\partial \f{x}_{cs} } + \tilde{f}_\g{t}
 \label{EQ:tank_input}
\end{equation}
where the first term of eq.~(\ref{EQ:tank_input}) represents the dissipated power. In order to have an open port to connect the tank to the controller, we introduce a new input $\tilde{f}_\g{t}$ to the energy tank. The energy balance of the tank is:
\begin{equation}\label{EQ:tankpower}
\dot{\mathcal{H}_t}(x_\g{t})=e_\g{t}f_\g{t} = \frac{\partial^T \mathcal{H}_{cs} }{\partial \f{x}_{cs} }D_c\frac{\partial \mathcal{H}_{cs} }{\partial \f{x}_{cs} } + x_\g{t}\tilde{f}_\g{t}
\end{equation}
%When $x_t = 0$, the energy tank is depleted and unable to passively perform the action commanded by the human, $T_{d,h}^0$. Thus, a complete depletion of the tank is avoided by introducing the switching parameter $\alpha$ which is a function of the tank level and is used to scale the commanded trajectory:
%\begin{equation}
%\alpha = 
%\begin{cases}
%1 & \mathcal{H}_t(x_t) \geq \mathcal{H}_t^{th} \\
%\frac{ \mathcal{H}_t(x_t) - \epsilon}{\mathcal{H}_t^{th}} & \epsilon \leq \mathcal{H}_t(x_t)  < \mathcal{H}_t^{th} \\
%0 & \mathcal{H}_t(x_t) < \epsilon
%\end{cases}
%\end{equation}
%%When connecting the tank port $(\tilde{f}_\g{t},e_\g{t})$ to the controller port $T_d^0,W_d^0$, the human is excluded from control loop. This addresses the energetic bounding of the system but prevents the human from issuing commands. 
We propose to interconnect the tank and the controller by a modulated transformer  $\mathbb{MTF}$~\cite{10stramigioli}. An $\mathbb{MTF}$ is a lossless element to dynamically shape the energy transfer by its variable transformer ratio $\f{n}$. The transformer ratio determines the energy flow and is set by the human to specify the desired velocity $T_{d,h}^0$. The $\mathbb{MTF}$ representation in the port-Hamiltonian framework is: 
%operator to control the power flow despite being energetically decoupled: 
\begin{eqnarray} \label{EQ:mtf}
\begin{aligned}
T_d^0 &= \f{n}e_\g{t} \\
\tilde{f}_\g{t} &= -\f{n}^TW_d^0
\end{aligned}
\end{eqnarray}
where $\f{n}$ is dynamically adapted to replicate the human command:
\begin{eqnarray}
 \f{n} = \frac{T_{d,h}^0}{x_\g{t}} 
\end{eqnarray}
%The relation~(\ref{EQ:mtf}) is power-conservative:
%\begin{equation}
%{T_d^0}^T W_v^0 = e_\g{t}^T\f{n}^T W_v^0 = -e_\g{t}^T\tilde{f}_\g{t}
%\end{equation}
%i.e. the transformer is lossless for any $\f{n}$.\\
%Let us assume the object to move freely in the workspace, i.e. $W_b^0  = 0$.
A combined port-Hamiltonian representation of the controlled system and the tank is as follows:
\begin{eqnarray}
\begin{aligned}
\begin{pmatrix}
\dot{\f{x}}_{cs} \\ \dot{x}_\g{t}
\end{pmatrix} =
\begin{pmatrix}
J_{cs}  & \frac{T_d^0}{x_\g{t}} \\ -\frac{{T_d^0}^T}{x_\g{t}}+\frac{1}{x_\g{t}}\frac{\partial^T \bar{\mathcal{H}}}{\partial \f{x}_{cs} }D_c & 0
\end{pmatrix}
\begin{pmatrix}
\frac{\partial \bar{\mathcal{H}}}{\partial \f{x}_{cs} } \\
\frac{\partial \bar{\mathcal{H}}}{\partial x_\g{t}}
\end{pmatrix}
\end{aligned}
\end{eqnarray}
The combined system is \emph{lossless} for free object motion , i.e. $W_b^0  = 0$,
\begin{eqnarray}
\begin{aligned}
&\frac{d}{dt}\bar{\mathcal{H}}(\f{x}_{cs} ,x_\g{t}) = 
\begin{pmatrix}
\frac{\partial^T \bar{\mathcal{H}}}{\partial \f{x}_{cs} } & \frac{\partial^T \bar{\mathcal{H}}}{\partial x_\g{t}}
\end{pmatrix}
\begin{pmatrix}
\dot{\f{x}}_{cs} \\ \dot{x}_{\g{t}}
\end{pmatrix}
= 0
\end{aligned}
\end{eqnarray}
where the Hamiltonian $\bar{\Ha}(\f{x}_{cs} ,x_\g{t})$ is the total energy of tank, controller and system.
%Note that there is no more an input-output port. 
%(unclear)This is because the external ports are defined by energy exchange of the system with its environment. 
\subsection{Energy shaped stiffness and damping}\label{SS:EnergyAdaptedStiffness}
When $x_t = 0$, the energy tank is depleted and unable to passively perform the action commanded by the human, $T_{d,h}^0$. A total depletion is avoided by suspending the input, i.e. $T_{d,h}^0 = 0$ if $x_t < \epsilon$, in a neighbourhood $\epsilon > 0$~\cite{10stramigioli}. Discarding the input results in a loss of information and a permanent set-point deviation. We propose to avoid a depletion of the energy tank without modifying the human input, but by making the coupling of set-point and virtual object velocity variable. By adapting the coupling stiffness and damping as a function of the available energy, the flow of energy between tank and controller is shaped. Stiffness and damping are constant if the energy in the tank is sufficient for executing the velocity command. If the tank level falls below a certain threshold, the coupling is relaxed to limit the power flow. 
%By easing the coupling (decreasing stiffness) the energy exchange is hindered. 
If the tank level is close to zero, $\Ha_\g{t}(\g{t}) < \epsilon$, the energy transfer is completely disabled by a zero stiffness and damping.
% By adapting the virtual stiffness depending on the energetic state of the energy tank the power flow is shaped.
%Actually the impedance of the human hand behaves similar, usually it is stiff for slow motion and compliant during fast movements (high kinetic energy, likely depleted energy tank) \cite{Hogan_84b}. 

%A lower stiffness allows the system to "float" loosely, while the tank gets refilled through the damping. 
A change of stiffness affects the energy stored in the spring. Relaxing stiffness sets energy free, increasing stiffness requires the supply of energy.
Let us assume an a stiffness-dependent Hamiltonian for a spring

%A change of stiffness affects the energy stored in the spring, relaxing stiffness sets energy free, which is re-supplied into the tank. With an increase of the tank energy level, the stiffness is increased. 

\begin{equation}
\Ha_\g{P}:SE(3) \times \mathcal{K} \rightarrow \mathbb{R}^+; (H_b^v,\kappa)\mapsto \Ha_\g{P}(H_b^v,\kappa),
\end{equation}
where $\kappa$ is the variable stiffness, a function of the tank level $\Ha_t$: above a certain threshold $\Ha_t^{th}$ the stiffness is constant. When the tank gets close to depletion the stiffness is reduced

\begin{equation}
\kappa = \begin{cases}
K_{dv} & \text{if } \, \mathcal{H}_t(x_\g{t})\geq \mathcal{H}_t^{th} \\
K_{dv} \frac{\mathcal{H}_t(x_\g{t})-\epsilon}{\mathcal{H}_t^{th}} & \text{if } \, \epsilon \leq \mathcal{H}_t(x_\g{t}) < \mathcal{H}_t^{th} \\
0 &\text{if } \, \mathcal{H}_t(x_\g{t}) < \epsilon
\end{cases},
\end{equation}
where $K_{dv} \in \mathbb{R}^{6 \times 6}$ is a stiffness matrix. The rate of energy change w.r.t a variable stiffness $\kappa$ is 
\begin{equation}
\dot{\mathcal{H}}_{\kappa} =\frac{\partial \mathcal{H}_{\kappa}}{\partial \kappa} \frac{d \kappa}{dt} = e_\kappa^T f_\kappa,
\end{equation}
which corresponds to the product of effort ($\frac{\partial \mathcal{H}_\g{P}}{\partial \kappa}$) and flow ($\dot{\kappa}$) and forms a power port $(f_\kappa,e_\kappa)$.
The port-Hamiltonian representation of a variable stiffness spring is given by
\begin{eqnarray}
\begin{aligned}
\begin{pmatrix}
\dot{H}_v^d \\ \dot{\kappa}
\end{pmatrix}
&=
\begin{pmatrix}
H_v^d Ad_{H_0^v} & 0 \\ 0 & I
\end{pmatrix}
\begin{pmatrix}
 T_v^{0}-T_d^0 \\ f_\g{k}
\end{pmatrix}
\\
\begin{pmatrix}
W_v^{0} \\ e_\g{k}
\end{pmatrix}
&=
\begin{pmatrix}
Ad_{H_0^v}^T {H_v^d}^T & 0 \\ 0 & I
\end{pmatrix}
\begin{pmatrix}
\frac{\partial \mathcal{H}_{\kappa}}{\partial H_v^d} \\ \frac{\partial \mathcal{H}_{\kappa}}{\partial \kappa}
\end{pmatrix}.
\end{aligned}
\end{eqnarray}




% With an increase of the tank energy level, the stiffness is increased. Let us illustrate the principle on an example of a one-dimensional spring:
%\begin{equation}
%F = k_{dv}(x_d-x_v) \;,\; \mathcal{H}_t(x_\g{t})\geq \mathcal{H}_t^{th}
%\end{equation}
%This equation is valid if the tank level $\mathcal{H}_t(x_\g{t})$ is above a certain threshold level $\mathcal{H}_t^{th}$. The stiffness of the spring $k_{dv} \in \mathbb{R}^+$ is constant and $x_d, \; x_v \in \mathbb{R}$ denote the reference and the actual displacement of the spring, respectively. Below the threshold the following spring function is proposed:
%\begin{equation}
%F = k_{dv}\frac{\mathcal{H}_t(x_{\g{t}})}{\mathcal{H}_t^{th}}(x_d-x_v) \;,\; \mathcal{H}_t(x_\g{t})<\mathcal{H}_t^{th}
%\end{equation}
%For the ease of notation a new stiffness parameter is introduced as:
%\begin{equation}
%\kappa = \begin{cases}
%k_{dv} & \text{if } \, \mathcal{H}_t(x_\g{t})\geq \mathcal{H}_t^{th} \\
%k_{dv} \frac{\mathcal{H}_t(x_\g{t})}{\mathcal{H}_t^{th}} & \text{if } \, \mathcal{H}_t(x_\g{t}) < \mathcal{H}_t^{th}
%\end{cases}
%\end{equation}
%The corresponding energy function of the variable spring is:
%\begin{equation}
%\mathcal{H}_{\kappa}(x_d,x_v,\kappa) = \frac{1}{2} \kappa (x_d-x_v)^2
%\end{equation}    
%and the exchanged power with respect to a change of stiffness is 
%\begin{equation}
%\dot{\mathcal{H}}_{\kappa} =\frac{\partial \mathcal{H}_{\kappa}}{\partial \kappa} \frac{d \kappa}{dt} = \frac{1}{2} (x_d-x_v)^2 \dot{\kappa} = e_\g{k}^Tf_\g{k},
%\end{equation}
%which corresponds to the product of effort ($\frac{\partial \mathcal{H}_{\kappa}}{\partial \kappa}$) and flow ($\dot{\kappa}$) and forms a power port $(f_\g{k},e_\g{k})$. The energy function of a $6$-DoF spring is of the from
%\begin{equation}
%V_{\kappa}:SE(3) \times \mathcal{K} \rightarrow \mathbb{R}^+; (H_b^v,\kappa)\mapsto V_{\kappa}(H_b^v,\kappa),
%\end{equation}
%which is equal to the previous spring energy function (\ref{EQ:SpringEnergyFunction}) but depends explicitly on the stiffness parameter $\kappa$.
%%$\mathcal{K}$ is a parametric space that equals $\mathbb{R}$ in case of an isotropic spring. For more information on variable spatial springs see \cite{Stramigioli_01c}. 
%The port-Hamiltonian representation of a variable stiffness spring is given by
%\begin{eqnarray}
%\begin{aligned}
%\begin{pmatrix}
%\dot{H}_v^d \\ \dot{\kappa}
%\end{pmatrix}
%&=
%\begin{pmatrix}
%H_v^d & 0 \\ 0 & I
%\end{pmatrix}
%\begin{pmatrix}
%T_v^{v,d} \\ f_\g{k}
%\end{pmatrix}
%\\
%\begin{pmatrix}
%W_v^{v,d} \\ e_\g{k}
%\end{pmatrix}
%&=
%\begin{pmatrix}
%{H_v^d}^T & 0 \\ 0 & I
%\end{pmatrix}
%\begin{pmatrix}
%\frac{\partial \mathcal{H}_{\kappa}}{\partial H_v^d} \\ \frac{\partial \mathcal{H}_{\kappa}}{\partial \kappa}
%\end{pmatrix}.
%\end{aligned}
%\end{eqnarray}
%where ($f_\g{k},e_\g{k}$) is the input-output pair.
One can express $\kappa$ as a function of the tank level using~(\ref{EQ:tankpower}):
\begin{equation}
\dot{\kappa} = \begin{cases}
0 & \text{if } \, \mathcal{H}_t(x_\g{t})\geq \mathcal{H}_t^{th} \\
\frac{K_{dv}}{\mathcal{H}_t^{th}}\dot{\mathcal{H}}_t(x_\g{t}) = \frac{K_{dv}}{\mathcal{H}_t^{th}} \dot{x}_\g{t} \frac{\partial \bar{\mathcal{H}}}{\partial x_\g{t}} & \text{if }  \, \mathcal{H}_t(x_\g{t}) < \mathcal{H}_t^{th}
\end{cases}
\end{equation}
The power exchanged between the variable spring and the tank due to variability of the stiffness is:
\begin{equation}
\dot{\bar{\mathcal{H}}} = \frac{\partial \bar{\mathcal{H}}}{\partial \kappa}\dot{\kappa} =
\begin{cases}
0 & \text{if } \, \mathcal{H}_t(x_\g{t})\geq \mathcal{H}_t^{th} \\
\frac{\partial \bar{\mathcal{H}}}{\partial \kappa}\frac{K_{dv}}{\mathcal{H}_t^{th}} \dot{x}_\g{t} \frac{\partial \bar{\mathcal{H}}}{\partial x_\g{t}} & \text{if }  \, \mathcal{H}_t(x_\g{t}) < \mathcal{H}_t^{th}
\end{cases}
\end{equation}

The power exchanged by the energy tank is of the form $\dot{\mathcal{H}_t}(x_\g{t}) = \frac{\bar{\mathcal{H}}}{\partial x_\g{t}}f_\g{t}$, therefore $\frac{\partial \bar{\mathcal{H}}}{\partial \kappa}\frac{K_{dv}}{\mathcal{H}_t^{th}} \dot{x}_\g{t}$ is an input for the energy tank. One can now obtain the port-Hamiltonian representation of the variable stiffness spring and the energy tank: 

%For simplicity, here the variable stiffness spring is part of the controller.
\begin{eqnarray}
\begin{aligned}
\begin{pmatrix}
\dot{\f{x}}_{cs}  \\ \dot{x}_\g{t} \\ \dot{\kappa}
\end{pmatrix} &=
\left[
\begin{pmatrix}
J_{cs}  & \frac{ T_d^0}{x_\g{t}} & 0\\ -\frac{{T_d^0}^T}{x_\g{t}} & 0 & -\frac{K_{dv}}{\mathcal{H}_\g{t}^{\g{th}}}\dot{x}_\g{t} \\ 0 & \frac{K_{dv}}{\mathcal{H}_{\g{th}}}\dot{x}_\g{t} & 0\end{pmatrix}
\right. \\ &- \left.
\begin{pmatrix}
0 & 0 & 0\\ -\frac{1}{x_\g{t}}\frac{\partial \bar{\mathcal{H}}^T}{\partial \f{x}_{cs} }D_c & 0 & 0 \\ 0 & 0 & 0
\end{pmatrix}
\right]
\begin{pmatrix}
\frac{\partial \bar{\mathcal{H}}}{\partial \f{x}_{cs} } \\
\frac{\partial \bar{\mathcal{H}}}{\partial x_\g{t}} \\
\frac{\partial \bar{\mathcal{H}}}{\partial \kappa} 
\end{pmatrix}
\end{aligned}
\end{eqnarray}
The combination of such a system with a variable stiffness spring and a tank is lossless:
%, disregarding the energy exchange with the robotic environment:
\begin{eqnarray}
\begin{aligned}
&\frac{d}{dt}\bar{\mathcal{H}}(\f{x}_{cs} ,x_\g{t},\kappa)
%\begin{pmatrix}
%\frac{\partial^T \bar{\mathcal{H}}}{\partial \f{x}_{cs} } & \frac{\partial^T \bar{\mathcal{H}}}{\partial x_\g{t}} &
%\frac{\partial^T \bar{\mathcal{H}}}{\partial \kappa}
%\end{pmatrix}
%\begin{pmatrix}
%\dot{\f{x}}_{cs} \\ \dot{x_\g{t}} \\ \dot{\kappa}
%\end{pmatrix}
= 0
\end{aligned}
\end{eqnarray}
The dissipative structure $D_v$ can be changed without compromising passivity as long as $D_v\succeq 0$. The mapping matrix $G_c$ in eq.~(\ref{EQ:pH_model_controller}) shows that the damping directly influences the energy exchange. We propose to reduce the damping in the same manner as for the stiffness. With a depletion of the energy tank the damping $D_{dv} \in \mathbb{R}^{6\times 6}$ parallel to the variable spring is reduced. The coupling between the human and the virtual object is relaxed:
\begin{equation}
\delta = \begin{cases}
D_{dv} & \text{if } \, \mathcal{H}_t(x_\g{t})\geq \mathcal{H}_t^{th} \\
D_{dv} \frac{\mathcal{H}_t(x_\g{t})}{\mathcal{H}_t^{th}} & \text{if } \, \epsilon \leq \mathcal{H}_t(x_\g{t}) < \mathcal{H}_t^{th} \\
0 &\text{if } \, \mathcal{H}_t(x_\g{t}) < \epsilon
\end{cases}
\end{equation} 
% In addition to the formation preservation and trajectory tracking capabilities of our extended IPC, we exploit the information on the energetic state of controller and system. By bounding the energy in the controller, the energy supply to the actual system also becomes bounded and entails in stable behaviour.\\
%allow for infeasible and/or unsafe trajectories. It is the objective of the controller to achieve safe and stable operation for arbitrary inputs.
% Since a controller defined in the port-Hamiltonian framework is intrinsically passive, an external source of energy is required to perform work and execute tasks. The human user issues a reference trajectory and receives force-feedback, i.e. there is an exchange of energy by the dual quantities velocity and force. Often the force-feedback provided does not restrict the human motion. Consider the example of robotic system driven into an obstacle. If the user persists to command motion in the blocked direction, then possibly an infinite amount of energy is supplied to the system. Next to trajectory tracking and formation preservation, the controller must also restrict the supply of energy to ensure stable operation. 