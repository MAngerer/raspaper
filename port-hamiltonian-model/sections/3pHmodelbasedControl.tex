
\section{port-Hamiltonian modelling of robot teams}\label{sec:modelling}
In this section we propose a port-Hamiltonian based model of a cooperative manipulation system. We represent it as a constrained model, in an implicit input-state-output form and derive the explicit input-state-output model. Let us assume a setting depicted in fig. (\ref{FIG:model}) where $N$ manipulators are connected to the common object. The inertial frame is denoted as $\Psi_0$, the object frame as $\Psi_b$ and the end-effector frames as $\Psi_{i}$ where $i=1,...,N$. The object and the end-effectors are represented as inertial elements, where the momentum of the object is $P_b^b$ and the momenta of the robots are $P_{b_i}^{b_i}$, $i=1,...,N$. The potential energy of the complete system is represented as a single spring, connecting the object and the ground, with the displacement denoted as $H_b^0$.
\begin{figure}[ht]
	\centering
	\footnotesize
	\def\svgwidth{0.8\columnwidth}
%	\includegraphics[]{model.eps}
    \input{pics/model.eps_tex}
	\caption{Cooperative system model represented as a set of interconnected energy storing elements, inertias and a spring.}
	\label{FIG:model}
	\vspace{-10pt}
\end{figure}

\subsection{Cooperative system modelling}
Let us represent the system depicted in fig.~(\ref{FIG:model}) in an implicit port-Hamiltoninan form:
\begin{eqnarray}\label{EQ:pHDAE}
\begin{aligned}
&\dot{\f{x}} = J(\f{x}) \frac{\partial \Ha_s}{\partial \f{x}} (\f{x}) + A(\f x) \f{\lambda} + G(\f x) \f{u}, \\
&0 = A^T(\f x) \frac{\partial \Ha_s}{\partial \f x}(\f x), \\
&\f y = G^T(\f x) \frac{\partial \Ha_s}{\partial \f x}(\partial x)
\end{aligned}
\end{eqnarray}
where $\f x \in \mathcal{M}$ is an $n$-dimensional state vector defined on a manifold, $J(\f x)$ is a skew-symmetric structure matrix, $A(\f x)$ is a constraint matrix with $\f{\lambda} \in \mathbb{R}^l$ being the constraining wrenches, $G(\f x)$ is the input mapping matrix, $\f u \in \mathcal{F}$ an $m$-dimensional input vector, $\f y \in \mathcal{F}^*$ an $m$-dimensional output vector and $\mathcal{H}_s$ is a Hamiltonian function of the model.\\
%The interconnection of inertia and spring accounting for the object is
%\begin{eqnarray} 
% \begin{aligned}
% \begin{pmatrix}\dot{H}_b^0 \\ \dot{P_b^b}\end{pmatrix} &=
% \begin{pmatrix} 0 & H_b^0  \\
% - (H_b^0)^T & C_b\end{pmatrix}
% \begin{pmatrix}\frac{\partial \mathcal{H}_b}{\partial H_b^0} \\ \frac{\partial \mathcal{H}_b}{\partial P_b^b}\end{pmatrix}+
% \begin{pmatrix}0 \\ I_6\end{pmatrix} W_{b}^b \\
% T_b^{b,0} &= \begin{pmatrix}0 & I_6\end{pmatrix}
% \begin{pmatrix}\frac{\partial \mathcal{H}_b}{\partial H_b^0} \\ \frac{\partial \mathcal{H}_b}{\partial P_b^b}\end{pmatrix},
% \end{aligned}
%\end{eqnarray}
%where $H_b^0$ denotes the displacement of the object w.r.t to ground.
The state $\f{x}$ in eq.~(\ref{EQ:pHDAE}) is a stacked vector of the configuration variable of the gravity spring and the momenta of the object and the $N$ manipulators: $\f{x}^T = [{H_b^0}^T, {P_b^b}^T,  {P_{b_1}^{b_1}}^T,..., {P_\bi^\bi}^T,..., {P_{b_N}^{b_N}}^T]^T$. The structure and the mapping matrices are
\begin{eqnarray}
\begin{aligned}
&J(\fx) = \begin{pmatrix}  0 & H_b^0 & 0 & \cdots & 0\\
 -{H_b^0}^T & C_b & 0 & \cdots & 0\\
 0 & 0 & C_{b_1} & \cdots & 0 \\
 \vdots & \vdots & \vdots & \ddots & \vdots \\
 0 & 0 & 0 & \cdots & C_{b_N}
 \end{pmatrix}, \\
& G(\fx) = \begin{pmatrix}
0 & 0 & \cdots & 0 \\ 
Ad_{H_0^b} & 0 & \cdots & 0\\ 
0 & Ad_{H_0^{b_1}} & \cdots & 0\\
 \vdots & \vdots & \ddots & \vdots \\
 0 & 0 & \cdots & Ad_{H_0^{b_N}} \\
\end{pmatrix} 
\end{aligned}
\end{eqnarray}
where $C_*$ accounts for the \emph{Coriolis} and centrifugal terms.
The stacked input vector consists of the external wrenches acting on the inertias $\f{u} = [W_b^0$, $W_{b_1}^0,...,W_\bi^0,...,W_{b_N}^0]$, expressed in the body-fixed frames. The outputs are the twists of the inertias $\f{y}^T= [{T_b^{0}}^T,{T_{b_1}^{0}}^T,...,{T_\bi^{0}}^T,...,,{T_{b_N}^{0}}^T]^T$.
The Hamiltonian energy $\Ha_s$ is the sum of the energies of all the elements:
\begin{eqnarray}
\Ha_s(\fx) = \Ha_\g{P}(H_b^0) + \Ha_\g{K}(P_b^b) + \sum_{i=1}^N \Ha_\g{K}(P_\bi^\bi).
\end{eqnarray}
The interaction of the manipulators with the rigid object imposes \emph{kinematic constraints} on the complete system. 
%Kinematic constraints are described by the \emph{constraint matrix} $A(\f{x}) \in \mathbb{R}^{k \times l}$ in an additional algebraic equation. The constraints violation generates counteracting forces expressed with the \emph{Lagrange} multipliers $\f{\lambda}$.
This implies that there is no relative motion between the object and the end-effectors which can be denoted as: 
\begin{eqnarray}
T_b^{0} = T_\bi^{0}, \; \forall i = 1..N 
\end{eqnarray}
where $T_b^{0}$ denotes the twist of the virtual object and $T_\bi^{0}$ stands for the twist of the $i$th manipulator. With a change of coordinates $T_b^{b,0} = Ad_{H_\bi^b} T_\bi^{\bi,0}$ the constraint equation is:
\begin{eqnarray}
0 = 
\underbrace{\begin{pmatrix}
0 & I_6 & -Ad_{H_{b_1}^b} & \cdots & 0 \\
\vdots & \vdots & \vdots & \ddots & \vdots \\
0 & I_6 & 0 & \cdots & -Ad_{H_{b_N}^b} 
\end{pmatrix}}_{A^T(\fx)}
\frac{\partial \Ha_s}{\partial \fx}(\fx)
%\; \forall i=1..N
\end{eqnarray}
where $A(\f{x}) \in \mathbb{R}^{n \times l}$ is the \emph{constraint} matrix with $n$ being the number of states of the system and $l$ the number of independent kinematic constraints.
The violation of constraints generates internal stress on the object~\cite{Erhart_16}. We consider the constraining forces using \emph{Lagrangian multipliers} $\f{\lambda}$. Therefore, the port-Hamiltonian formulation of the constrained model is a set of differential and algebraic equations. To restore the explicit input-state-output form (\ref{EQ:generalPHS}), the implicit model needs to be restricted to the constrained manifold $\mathcal{M}$ which can be achieved by eliminating the Lagrangian multipliers from eq.~(\ref{EQ:pHDAE}).

\subsection{Elimination of constraints}
The differential-algebraic equation~(\ref{EQ:pHDAE}) can be reduced to a set of ordinary differential equations by multiplying it with a full-rank left annihilator of the constraint matrix, $A^\bot(\fx)$, such that $A^\bot(\fx) A(\fx) \f{\lambda} = 0$. The annihilator can be calculated as the kernel of $A(x)$. We use the left annihilator of the following form:
\begin{eqnarray}
A^\bot(\fx) = \begin{pmatrix*}[c]
I_6 & 0_6 & 0_6 & \cdots & 0_6 \\
0_6 & \underbrace{\begin{smallmatrix}I_3 & \tilde{\f{p}}_b^0 \\ 0_3 & I_3 \end{smallmatrix}}_{\mathcal{G}_b} & \underbrace{\begin{smallmatrix} I_3 & \tilde{\f{p}}_{b_1}^0 \\ 0_3 & I_3\end{smallmatrix}}_{\mathcal{G}_{{b}_1}} & \cdots &  \underbrace{\begin{smallmatrix} I_3 & \tilde{\f{p}}_{b_N}^0 \\ 0_3 & I_3\end{smallmatrix}}_{\mathcal{G}_{{b}_N}}  
\end{pmatrix*}
\label{EQ:annihilator}
\end{eqnarray}
where the combined matrix $\mathcal{G} = [\mathcal{G}_b,\mathcal{G}_{b_1},...,\mathcal{G}_{b_N}] \in \mathbb{R}^{6 \times 6(N+1)}$ is the \emph{grasp matrix}. Multiplying eq. (\ref{EQ:pHDAE}) with the annihilator~(\ref{EQ:annihilator}) the following is obtained:
\begin{eqnarray}
\begin{aligned}
A^\bot \dot{\fx} &= A^\bot J(\fx)\frac{\partial \Ha_s}{\partial \fx} (\f{x}) + A^\bot G(\f x) \f{u} %\\
%\f{y} &= G^T(\fx) \frac{\partial \Ha_s}{\partial \hat{\fx}}
\end{aligned}
\end{eqnarray}
Effectively, $A^\bot \dot{\fx}$ is a coordinate transformation \cite{Schaft_13}. With the proposed annihilator of the constraint matrix, $A^\bot$, the configuration $H_b^0$ is unaffected by the transformation.
%\begin{eqnarray}
%A^\bot = \begin{pmatrix}
%I_6 & 0_{6 \times 6(N+1)} \\
%0_6 & \mathcal{G}\end{pmatrix}.
%\end{eqnarray}
For the momenta a new variable $\hat{P}_b^b = \mathcal{G} [{P_b^b}^T, {P_\bi^\bi}^T]^T$ is introduced. The resulting state $\hat{\fx}=[{H_b^0}^T,{\hat{P}_b^b}{}^T]^T$ evolves on a constrained manifold:
\begin{eqnarray}
 \mathcal{M}_c = \{\fx \in \mathcal{M} | A^T(\fx) \frac{\partial \Ha_s}{\partial \f{x}}=0\}.
 \label{EQ:constr_manifold}
\end{eqnarray}
The dimension of the state vector is reduced in the constrained space, $\hat{\fx} \in \mathbb{R}^{n-6N}$.
 %as a consequence of the uniform motion of object and manipulators. 
 The partial derivatives of the Hamiltonian function are modified to $\frac{\partial \Ha_s}{\partial \fx} = {A^\bot}^T \frac{\partial \Ha_s}{\partial \hat{\fx}}$. Now it is possible to obtain the model in an explicit input-state-output form:
\begin{eqnarray} \label{EQ:epH}
\begin{aligned}
\dot{\hat{\fx}} &= \hat{J}({\fx}) \frac{\partial \hat{\Ha}_s}{\partial \hat{\fx}} + \hat{G}(\fx) \f{u} \\
\f{y} &= \hat{G}^T (\fx) \frac{\partial \hat{\Ha}_s}{\partial \hat{\fx}}
\end{aligned}
\end{eqnarray}
where $\hat{\Ha}_s$ is the Hamiltonian function evolving on the constrained manifold~(\ref{EQ:constr_manifold}). The reduced structure matrix $\hat{J}(\fx) = {A^\bot} J(\fx) {A^\bot}^T$ is again skew-symmetric, i.e. we obtain the explicit input-state-output port-Hamiltonian representation of the constrained system.
The new mapping matrix $\tilde{G}(x)=[\tilde{G}_b \; \tilde{G}_\g{m}]$ decomposes into two parts: $\tilde{G}_b \in \mathbb{R}^{12 \times 6}$ represents the interaction of the object with the environment. $\tilde{G}_\g{m}\in \mathbb{R}^{12 \times 6N}$ represents the interaction with the controller. 
 The energy balance of~(\ref{EQ:epH}) is $\dot{\hat{\Ha}}_s= \f{y}^T\f{u}$, i.e. the system is power-conservative.