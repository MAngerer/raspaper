\section{port-Hamiltoninan model-based control}\label{S:ControlDesign}
In this section we propose a model-based controller of a cooperative manipulation system in the port-Hamiltonian framework. Let us assume a setting depicted in fig. (\ref{FIG:admittancecontrol}) where $N$ manipulators are rigidly connected to the common object. The inertial frame is denoted as $\Psi_0$, the object frame as $\Psi_b$ and the end-effector frames as $\Psi_{i}$ where $i=1,...,N$. Additionally, we define frames at contact points of the object as $\Psi_{b_i}$ and the frame on the human palm as $\Psi_v$.\\
%The robots need to preserve a certain formation to avoid dropping or excessive squeezing of the object. \\
A model based controller is described in the port-Hamiltonian framework as an interconnection of virtual mechanical elements. It is suitable for achieving both trajectory tracking and formation preservation of the robots. The formation preservation ensures that the excessive stress is not applied on the object.
\subsection{Model-based control design}
The modelling of a cooperative set-up starts from the common object that is grasped by the robot manipulators. The object is represented by a simple inertia of the form (\ref{EQ:pHInertia}). Let us assume a rigid connection between object and end-effectors. 
For each robot a simple inertia, associated with frame $\Psi_\bi$, is added directly to the object (see figure \ref{FIG:admittancecontrol}). Real robots have multiple joints and links and thus an effective inertia that is configuration dependent. Feedback-linearisation techniques \cite{Ott_04} compensate for the internal dynamic in favour of shaping a desired inertia. The assumption of a non-configuration dependent manipulation inertia is reasonable, if a perfect feedback-linearisation is implemented on the robot.\\
We extend the simple model of the cooperative set-up by springs and dampers to compliantly couple the user and the robots. A virtual spring and a damper reach from the virtual object to the commanded reference pose, which is indicated by the palm of the user. Every robot end-effector is coupled by a virtual spring and a damper to the surface of the virtual object. The extent of the virtual object defines the formation of the robot.\\ 
%A suitable (in terms of input-output causality) hinge point expects a wrench as the input and outputs a twist.
Therefore we obtain an (virtual) object centred control scheme. The concept of the \emph{virtual object} originates from the IPC approach applied so far to the robotic hand control. We extend the classical IPC by introducing dynamical virtual connections between the end-effectors and the object modelled by a stiffness, damper and inertia elements as depicted in fig.~\ref{FIG:admittancecontrol}.
\begin{figure}
	\centering
	\footnotesize
	\def\svgwidth{0.9\columnwidth}
	%\includegraphics[]{admittancecontrol.eps}
    \input{pics/admittancecontrol.eps_tex}
	\caption{Virtual structure of the constrained dynamic IPC (dampers along the springs not shown): The virtual object $b$ in the centre is connected with springs (and dampers) to the human hand frame $\Psi_v$ and to each end-effector associated with frames $\Psi_i, \, i=1..N$. Every end-effector is modelled with a virtual inertia $\bi$ attached rigidly to the object $b$.  }
	\label{FIG:admittancecontrol}
	\vspace{-10pt}
\end{figure}
%dynamical nature of the controller
In addition to the formation preservation and trajectory tracking capabilities of our extended IPC, we exploit the information on the energetic state of the controller. By bounding the energy in the controller, the energy supply to the actual system also becomes bounded and entails in stable behaviour.\\
%If the properties of the virtual object are chosen close to the actual, aproximat similar amount of energy is stored in the system and the controller. Thereby the controller becomes a coarse energetic model. 
% There are two major challenges to be considered in this type of human-robot team interaction: $1$) How to address the coordination of the multiple robots in order to let the operator focus on object motion; 2) How to achieve stability with the human in the loop.\\
%Towards stability and safety the major challenge is the largely unknown dynamics of both human and environment.
%allow for infeasible and/or unsafe trajectories. It is the objective of the controller to achieve safe and stable operation for arbitrary inputs.
% Since a controller defined in the port-Hamiltonian framework is intrinsically passive, an external source of energy is required to perform work and execute tasks. The human user issues a reference trajectory and receives force-feedback, i.e. there is an exchange of energy by the dual quantities velocity and force. Often the force-feedback provided does not restrict the human motion. Consider the example of robotic system driven into an obstacle. If the user persists to command motion in the blocked direction, then possibly an infinite amount of energy is supplied to the system. Next to trajectory tracking and formation preservation, the controller must also restrict the supply of energy to ensure stable operation. 
%Thus the controller has three tasks, $1$) to ensure trajectory following with respect to the human motion, $2$) to generate suitable trajectories for each robot to respect the coordination requirements.  $3$) preserve stability regardless the commanded trajectory.
%The controller output are wrenches to be executed by the robots. Force-feedback can be provided to the user while the controller receives the actual twist of the robots. Springs and dampers are suitable to interconnect the human hand and the robots and to establish a compliant coupling. The springs and dampers can either connect each component, or from each component to a virtual hinge point in the centre.

\subsection{port-Hamiltonian formulation of the controller}
The controller is formulated in the pH framework by interconnecting the elements, defined in subsection \ref{SS:pHElements}, to form a virtual structure.
The connection of mechanical pH elements follows \emph{Newton's} third law (action = -reaction). The interconnection of a spring and an inertia is the ideal pair in terms of input-output causality. The spring expects a twist-input and outputs a wrench. The inertia has a wrench-input and outputs a twist. The rigid coupling of two inertias makes them move uniformly, i.e. a \emph{kinematic constraint} is imposed. Kinematic constraints cannot be described by interconnecting pH systems but are expressed with an additional algebraic equation. The virtual manipulator inertias have a constant distance and orientation w.r.t to the virtual object, i.e. $T_b^{0,0} = T_\bi^{0,0}, \; \forall i = 1..N$. Here $T_b^{0,0}$ denotes the twist of the virtual object and $T_\bi^{0,0}$ stands for the twist of the $i$th of $N$ virtual manipulators. The kinematic constraints are expressed in matrix vector form:
\begin{eqnarray}
\begin{pmatrix}
I_6  &  -I_6
\end{pmatrix}
\begin{pmatrix}
T_b^{0,0} \\ T_\bi^{0,0}
\end{pmatrix}
= A^T T = 0, \; \forall i=1..N
\end{eqnarray}
where $A(\f{x}) \in \mathbb{R}^{k \times l}$ is the \emph{constraint} matrix with $k$ states of the system and $l$ the number of independent kinematic constraints.\\
%The pH-formulation of an inertia outputs a twist (eq. (\ref{EQ:pHInertia})) in body-fixed coordinates, thus the constraints are properly expressed as
%\begin{eqnarray}
%A^T \begin{pmatrix}T_b^0 \\ T_\bi^0\end{pmatrix}
% = \begin{pmatrix} I_6 & -I_6 \end{pmatrix}\begin{pmatrix}Ad_{H_b^0} & 0 \\ 0 & Ad_{H_\bi^0}\end{pmatrix}\begin{pmatrix}
%\frac{\partial \Ha_\g{K}}{\partial P_b^b} \\ \frac{\partial \Ha_\g{K}}{\partial P_\bi^\bi}\end{pmatrix} = 0 
%\end{eqnarray}
%Hamiltonian function necessary!!!
The constraints violation generates internal stress on the object if a relative motion between the inertias is attempted. We consider the constraining forces using \emph{Lagrange multipliers} $\f{\lambda}$. The port-Hamiltonian formulation of the controller with the constraints is a mixed set of differential and algebraic equations. Therefore, the system is no longer described in the input-state-output form:
%The additional condition to be fulfilled brings the system to a semi-explicit form \cite{Schaft_13}.
\begin{eqnarray}\label{EQ:constrainedPHS}
\begin{aligned}
\dot{\f{x}} &= [J(\f{x})-D(\f{x})]\frac{\partial \mathcal{H}}{\partial \f{x}}(\f{x}) + G(\f{x})\f{u} + \bar{A}(\f{x})\f{\lambda}\\
\f{y} &= G^T(\f{x})\frac{\partial \mathcal{H}}{\partial \f{x}}(\f{x})\\
0 &= \bar{A}^T(\f{x})\frac{\partial \mathcal{H}}{\partial \f{x}} 
\end{aligned}
\end{eqnarray}
with the components
\begin{gather}
\begin{aligned}
&\dot{\f{x}}= \begin{pmatrix*}[l] \dot{H}_b^v \\  \dot{P_b^b} \\ \dot{H}_\bi^i \\ \dot{P}_\bi^\bi\end{pmatrix*}, \; \frac{\partial \mathcal{H}}{\partial x} = \begin{pmatrix}\frac{\partial \Ha}{\partial H_b^v} \\ \frac{\partial \Ha}{\partial P_b^b} \\ \frac{\partial \Ha}{\partial H_\bi^i} \\ 
\frac{\partial \Ha}{\partial P_\bi^\bi}\end{pmatrix}, \; \f{u} = \begin{pmatrix}
T_v^0 \\ T_i^0
\end{pmatrix}, \; \f{y} = \begin{pmatrix}
W_v^{0} \\ W_i^{0}
\end{pmatrix},\\
&J(x) = \begin{pmatrix}  0 & H_b^v & 0 & 0 \\
 -{H_b^v}^T & C_b & 0 & 0 \\
 0 & 0 & 0 & H_\bi^i \\
 0 & 0  & -{H_\bi^i}^T & C_\bi \end{pmatrix}, \\
 &D(x) = \begin{pmatrix}  0 & 0 & 0 & 0 \\
 0 & D_b & 0 & 0 \\
 0 & 0 & 0 & 0 \\
 0 & 0 & 0 & D_\bi\end{pmatrix}, \\
 &G(x)=\begin{pmatrix}
-H_b^v Ad_{H_0^b} & 0 \\
 D_b Ad_{H_0^b} & 0 \\
0 & -H_\bi^i Ad_{H_0^\bi} \\
0 & D_\bi Ad_{H_0^\bi}
\end{pmatrix}, \;
\bar{A} = \begin{pmatrix}
0 \\ Ad_{H_b^0}^T \\ 0 \\ -Ad_{H_i^0}^T
\end{pmatrix}
\end{aligned}
\raisetag{10em} 
\end{gather}. 
The state vector $\fx$ is a stacked vector consisting of the states of the
\begin{enumerate}
\item virtual spring between the human hand and the virtual object ${H}_b^v$,
\item the virtual object $b$ momenta $P_b^b$,
\item the virtual spring between the virtual object $\bi$ and the end-effector $i$ ${H}_\bi^i$ and
\item the momenta of the virtual manipulator inertia ${P}_\bi$.
\end{enumerate}
The Hamiltonian energy of the interconnected system is the sum of all the elements
\begin{eqnarray}
\Ha(\fx) = \Ha_\g{P}(H_v^b) + \sum_{i=1}^N \Ha_\g{P}(H_\bi^i) + \Ha_\g{K}(P_b^b) + \sum_{i=1}^N \Ha_\g{K}(P_\bi^\bi).
\end{eqnarray}
%(unclear)The structure matrix $J(x)$ shows no interconnection between the virtual object and virtual manipulator, the coupling is described in the constraint equation $0=\bar{A}^T \partial \Ha / \partial \fx$.

\subsection{Elimination of constraints}
In order to restore the explicit input-state-output form~(\ref{EQ:generalPHS}) the \emph{Lagrange} multipliers have to be eliminated. One method \cite{Schaft_13} starts by differentiating the constraints w.r.t. time
\begin{equation}\label{EQ:constraintderivative}
0 = \frac{d}{dt}\left(\bar{A}^T(\f{x})\frac{\partial \mathcal{H}}{\partial \f{x}}\right) = \frac{d}{dt}(Ad_{H_b^0} T_b^{b,0} - Ad_{H_\bi^0} T_\bi^{\bi,0}).
\end{equation}
The time-derivative of the coordinate change $Ad_{H_b^0}$ is computed as $\frac{d}{dt}Ad_{H_b^0} = Ad_{H_b^0}ad_{T_b^{b,0}}$ where $ad_{T_b^{b,0}}$ is the \emph{adjoint} mapping:
\begin{eqnarray}\label{EQ:adjointmapping}
ad_{T_b^{b,0}} = \begin{pmatrix}
\tilde{\omega}_b^{b,0} & 0 \\ \tilde{v_b^{b,0}} & \tilde{\omega}_b^{b,0}\end{pmatrix}
\end{eqnarray}
Computing the products $ad_{T_b^{b,0}} T_b^{b,0} = ad_{T_{b_i}^{b_i,0}} T_{b_i}^{b_i,0} = 0$ simplifies~(\ref{EQ:constraintderivative}) to 
$0 = Ad_{H_b^0} \dot{T}_b^{b,0} - Ad_{H_\bi^0} \dot{T}_\bi^{\bi,0}$.
The twists can be expressed as a product of the inverse inertia and the momentum of the body, i.e. $T_b^{b,0} = M_b^{-1} P_b^b, \, T_\bi^{\bi,0} = M_\bi^{-1} P_\bi^\bi$. The inertia matrices $ M_b, \, M_\bi$, do not change with the state of the system, i.e. are not configuration dependent. The time-derivative of the constraints is thus $0 = Ad_{H_b^0} M_b^{-1} \dot{P}_b^{b} - Ad_{H_\bi^0} M_\bi^{-1} \dot{P}_\bi^{\bi}$. The time derivatives of momenta $\dot{P}_b^{b}, \, \dot{P}_\bi^{\bi}$ are described in~(\ref{EQ:constrainedPHS}). After inserting the expression for $\dot{P}_b^{b}, \, \dot{P}_\bi^{\bi}$ into the constraint equation, we obtain:
%\begin{eqnarray}
%0 = Ad_{H_b^0} M_b^{-1} ((C_b-D_b) \frac{\partial \Ha}{\partial P_b^b} -{H_b^v}^T  \frac{\partial \Ha}{\partial H_b^v}) + D_b Ad_{H_b^0} T_v^{0,0} + Ad_{H_b^0} \lambda - Ad_{H_\bi^0} M_b^{-1} ((C_b-D_b) \frac{\partial \Ha}{\partial P_b^b} -{H_b^v}^T  \frac{\partial \Ha}{\partial H_b^v}) + D_b Ad_{H_b^0} T_v^{0,0} + Ad_{H_b^0} \lambda
%\end{eqnarray}
\begin{eqnarray}
\begin{aligned}
0 &= \begin{pmatrix}
Ad_{H_b^0} M_b^{-1} & - Ad_{H_\bi^0} M_\bi^{-1}
\end{pmatrix}\\
&\left[\begin{pmatrix}
 -{H_b^v}^T & C_b-D_b & 0 & 0 \\
 0 & 0  & -{H_\bi^i}^T & C_\bi-D_\bi
\end{pmatrix}
\frac{\partial \mathcal{H}}{\partial x}\right.\\
& \left. + \begin{pmatrix}
 D_b Ad_{H_0^b} & 0 \\
0 & D_\bi Ad_{H_0^\bi}
\end{pmatrix} \f{u} +
\begin{pmatrix}
Ad_{H_b^0}^T \\ -Ad_{H_\bi^0}^T
\end{pmatrix} \lambda \right]
\end{aligned}
\end{eqnarray}
Solving this formula for $\lambda$ gives:
\begin{eqnarray} \label{EQ:lambda_solved}
\lambda = \mathcal{A}_x\frac{\partial \mathcal{H}}{\partial x} + \mathcal{A}_u \f{u}
\end{eqnarray}
Replacing $\lambda$ from~(\ref{EQ:lambda_solved}) into the~(\ref{EQ:constrainedPHS}) restores the input-state-output form:
\begin{eqnarray}
\begin{aligned}
\dot{\f{x}} &= [J(\f{x})-D(\f{x})+ \bar{A}\mathcal{A}_x]\frac{\partial \mathcal{H}}{\partial \f{x}} + [G(\f{x})+\bar{A}\mathcal{A}_u]\f{u}\\
\f{y} &= [G^T(\f{x})+\mathcal{A}_u^T \bar{A}^T]\frac{\partial \mathcal{H}}{\partial \f{x}}
\end{aligned}
\end{eqnarray}
It can be shown that the kinematic constraints do not affect the energy balance of the system by calculating
\begin{eqnarray}
\frac{\partial^T \mathcal{H}}{\partial \f{x}} \bar{A}\cA_{\fx}  \frac{\partial \mathcal{H}}{\partial \f{x}} = 0
\end{eqnarray}
%Do we need to proof this? Maybe in an appendix?
%The proof is in my thesis but the notation is different.
The controller is therefore passive and we call it a~\emph{constrained dynamic intrinsically passive controller (constrained dIPC)}. Due to the passivity an external source is necessary to perform work and execute tasks. This role is taken by the human operator. The next section proposes an energy regulation strategy to guarantee stability regardless of the behaviour of the humans. 