\section{Theoretical background}
\subsection{port-Hamiltonian framework}
Every physical process results in transformation or transfer of energy. The port-Hamiltonian modelling framework is based on the known energy function, a \emph{Hamiltonian}($\mathcal{H}$), and provides the energy-consistent description of a physical system~\cite{vanderSchaft_06}. Furthermore, as the interconnection of port-Hamiltonian systems is again a port-Hamiltonian system, this framework is suitable for modelling of complex and interconnected systems.\\  
Complex mechanical systems can be modelled as port-Hamiltonian system starting from atomic mechanical elements: masses, dampers and springs. Every atomic element is defined by a Hamiltonian energy function and interacts with the other elements by exchanging energy through a \emph{port}. The port is described by a pair of variables $(e, f)$ representing \emph{flows} and \emph{efforts}, respectively. They are power-conjugate, i.e. their dual product is power.\\ 
An input-state-output form of a general port-Hamiltonian system with the Hamiltonian function given as $\mathcal{H}$ is:
\begin{eqnarray}\label{EQ:generalPHS}
\begin{aligned}
\dot{\f{x}} &= [J(\f{x})-D(\f{x})]\frac{\partial \mathcal{H}}{\partial \f{x}}(\f{x}) + G(\f{x})\f{u}\\
\f{y} &= G^T(\f{x})\frac{\partial \mathcal{H}}{\partial \f{x}}(\f{x}),
\end{aligned}
\end{eqnarray}
where $J(\f{x})$ is a skew-symmetric structure matrix and $D(\f{x})  \succeq 0$ is a symmetric dissipation matrix. Inputs to the system are flows, $\f{u}$, and outputs are efforts $\f{y}$. Matrix $G(\f{x})$ is a mapping matrix and $\f{x}$ is the state vector of the system.\\
The Hamiltonian is a positive semi-definite function $\mathcal{H} \geq 0 $, which represents the total energy stored in the system. Since a port-Hamiltonian system either stores or dissipates supplied energy, the energy balance of~(\ref{EQ:generalPHS}) is:
\begin{eqnarray}\label{EQ:E_balance_generalPHS}
\dot{\Ha} = \f{y}^T\f{u} - \frac{\partial^T \mathcal{H}}{\partial \f{x}}D(\f{x})\frac{\partial \mathcal{H}}{\partial \f{x}}
\end{eqnarray}
Based on~(\ref{EQ:E_balance_generalPHS}) every port-Hamiltonian system is passive. Passivity is a sufficient criterion for asymptotic stability. Therefore, a controller formulated in the port-Hamiltonian framework is passive and achieves a closed-loop stability with any system that is a bounded source of energy~\cite{Stramigioli_15}.\\
\begin{figure}
			\centering
			\def\svgwidth{0.6\columnwidth}
            \footnotesize
			%\includegraphics[width=0.6\columnwidth]{virtualstructure.eps}
			\input{pics/virtualstructure.eps_tex}
            \caption{The controller as virtual structure connecting user input and robots}
\end{figure}\label{FIG:VirtualStructure}
%The interconnection of pH systems is power-conservative and is again a pH system. 
%\subsection{Virtual structures}
%Mechanical elements in the controller domain can be combined to achieve a desired robot behaviour. Coupling two bodies (e.g. common object and robot) by a virtual spring leads to compliant behaviour. The compliance relation extends to first-order impedance control by adding a damper parallel to the spring. There are two alternatives to establish a desired formation of robots around the held object: $1$) the use of non-zero rest-length springs connecting the robots pairwise, or $2$) the introduction of a virtual object as a hinge point (see figure ??). The second alternative has the advantage of leading to an object-centred control scheme. The reference trajectory issued by human command is connected by a spring to the virtual object. In the following the atomic mechanical elements are introduced in the port-Hamiltonian formulation.
\subsection{Twists and wrenches}
The port-Hamiltonian formulation uses coordinate frames associated with a body, i.e. non-inertial frames. A transformation from the inertial frame, $\Psi_0$, to a frame $\Psi_i$ is given by a homogeneous matrix
\begin{eqnarray}\label{EQ:coordchange}
	H_0^i := \begin{pmatrix*}[l]R_0^i & \f{p}_0^i \\ \f{0}_{1\times3} & 1\end{pmatrix*} \in SE(3)
\end{eqnarray}
which consists of a linear displacement vector $\f{p}_0^i \in \mathbb{R}^3$ and a rotation matrix $ \g{R}_0^i \in SO(3)$.\\
In this paper we represent the rigid body motion with the twist notation (see e.g. \cite{Stramigioli_01}). A twist is a motion consisting of a rotation around a screw axis and a translation along the same axis:
\begin{eqnarray}
T_i^{0,j} = \begin{pmatrix} \f{\omega}_i^{0,j} \\ \f{v}_i^{0,j}\end{pmatrix}
\end{eqnarray}
and is the motion of a body described with the frame $\Psi_i$, relative to the frame $\Psi_j$ and expressed in the inertial frame $\Psi_0$. The angular velocity is $\f{\omega}_i^{0,j} \in \mathbb{R}^3$ and the translational velocity is $\f{v}_i^{0,j} \in \mathbb{R}^3$. A coordinate transformation of a twist from $\Psi_0$ to $\Psi_j$ is given by
\begin{eqnarray}
T_i^{j,j} = T_i^j = Ad_{H_0^j} T_i^{0,j} = \begin{pmatrix}R_0^j & 0 \\ \tilde{p}_0^j R_0^j & R_0^j\end{pmatrix} T_i^{0,j},
\end{eqnarray}
where $\tilde{p}_0^j$ is the skew-symmetric matrix of the $\f{p}_0^j$. The dual quantity of a twist is a \emph{wrench}. The wrench $W_i^0$ acts on a body associated with the frame $\Psi_i$, expressed in the inertial frame $\Psi_0$: 
\begin{eqnarray}
W_i^0 = \begin{pmatrix} \f{m}_i^0 \\ \f{f}_i^0 \end{pmatrix}
\end{eqnarray}
where $ \f{{m}} \in \mathbb{R}^3 $ are moments and $ \f{f} \in \mathbb{R}^3$ forces. A coordinate transformation of wrenches from frame $0$ to frame $j$ is defined as:
\begin{eqnarray}
W_i^j = Ad_{H_j^0}^T W_i^0
\end{eqnarray}

\subsection{port-Hamiltonian formulation of mechanical elements}\label{SS:pHElements}
The controller proposed in this paper is based on an interconnection of virtual mechanical elements. Therefore, let us define these elements in the port-Hamiltonian form.
\subsubsection{Springs}
A \emph{spring} is the ideal, lossless element storing potential energy. It is connected to two bodies and is defined by a potential energy function. 
%The energy is a function of the relative displacement of the attached bodies. 
Let us consider a spring between the two bodies $ B_i $ and $ B_j $,  with coordinate frames $ \Psi_i $ and $ \Psi_j $ fixed to the respective bodies. The stored potential energy is a positive definite function of the $3D$ displacement $H_i^j$:
\begin{equation}\label{EQ:SpringEnergyFunction}
	\Ha_{\g{P}} : SE(3) \rightarrow \mathbb{R}; \; H_i^j \mapsto \Ha_{\g{P}}(H_i^j).
\end{equation}
For explicit energy functions for different types of springs see \cite{Stramigioli_01}.
The input-state-output form is defined by the displacement $H_i^j$ (state variable), the wrench $W_i^{j}$ (output) and the twist $T_i^j$ (input)
\begin{eqnarray}
\begin{aligned}
	&\dot{H}_i^j = T_i^{j}H_i^j\\
	&W_i^{j} = \frac{\partial \Ha_{\g{P}}(H_i^j)}{\partial H_i^j}(H_i^j)^T.
\end{aligned}
\end{eqnarray}
\subsubsection{Inertias}
Moving inertias possess kinetic energy, which is a function of the relative motion w.r.t. to an inertial reference frame
\begin{eqnarray}
\Ha_\g{K}(P_b^b) = \frac{1}{2}(P_b^b)^T M_b^{-1} P_b^b
\end{eqnarray}
where $P_b^b$ is the momentum of a body $b$ expressed in a body-fixed frame $\Psi_b$.
The dynamic properties of the body are defined by its inertia matrix $M_b$. 
%twist and momentum are related by $P_b^b = M_b T_b^{b,0}$. Whenever motion is expressed in a non-inertial frame fictitious forces (Coriolis, centrifugal) need to be considered. 
The equations of motion for a rigid body expressed in frame $\Psi_b$ are
\begin{eqnarray}\label{EQ:pHInertia}
\begin{aligned}
	&\dot{P_b^b} = C_b \frac{\partial \Ha_\g{K}(P_b^b)}{\partial P_b^b} + I_6 W_{b}^b \\
	&T_b^{b,0} = I_6 \frac{\partial \Ha_\g{K}(P_b^b)}{\partial P_b^b}
\end{aligned}
\end{eqnarray}
where $C_b$ represents Coriolis and centrifugal forces. The external wrench $W_b^b$ is the input and the output is the body's twist $T_b^{b,0}$. The state variable of an inertia is the momentum $P_b^b$.

\subsubsection{Dampers}\label{SSS:Dampers}
Let us assume linear damping, $D$, such that the wrench is directly proportional to twist. Consider the motion of the body $b$ with respect to the inertial frame as:
\begin{equation}
	W_b^b = D T_b^{b,0},
\end{equation}
or a damper in parallel with spring
\begin{equation}
	W_i^{j,j} = D T_i^{j}.
\end{equation}
The dissipated co-energy is:
\begin{eqnarray}
\Ha_\g{D}^* = \frac{1}{2}{T_b^{b,0}}^T D T_b^{b,0}.
\end{eqnarray}